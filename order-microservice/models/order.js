const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema({
  productId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Product",
    required: true,
  },
  orderDate: { type: Date, required: true },
  isCompleted: { type: Boolean, default: false },
});

const OrderModel = mongoose.model("Order", OrderSchema);

module.exports = OrderModel;
