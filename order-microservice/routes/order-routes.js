const Router = require("koa-router");

const {
  updateOrder,
  getOrderById,
  createOrder,
} = require("../controllers/order-controller");

const router = new Router();

router.post("/orders", createOrder);
router.get("/orders/:id", getOrderById);
router.put("/orders/:id", updateOrder);

module.exports = router;
