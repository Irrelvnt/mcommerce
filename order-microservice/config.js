const config = {
  dbConnectionString:
    process.env.MONGO_URI || "mongodb://localhost:27017/order",
};

export default config;
