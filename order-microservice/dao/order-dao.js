// /src/dao/ProductDao.ts

const OrderModel = require("../models/order");

class OrderDao {
  findById(id) {
    return OrderModel.findById(id).exec();
  }
  create(order) {
    return OrderModel.create(order);
  }
  update(id, order) {
    return OrderModel.findByIdAndUpdate(id, order, { new: true }).exec();
  }
}

module.exports = new OrderDao();
