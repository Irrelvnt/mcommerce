const Koa = require("koa");
const orderDao = require("../dao/order-dao");

/**
 * @param {Koa.Context} ctx
 */

const updateOrder = async (ctx) => {
  try {
    const order = await orderDao.update(ctx.params.id, ctx.request.body);

    if (!order) {
      ctx.status = 404;
      ctx.body = "Order not found";
      return;
    }

    ctx.status = 200;
    ctx.body = order;
  } catch (error) {
    ctx.status = 500;
    ctx.body = "Internal Server Error";
  }
};
/**
 * @param {Koa.Context} ctx
 */
const getOrderById = async (ctx) => {
  try {
    const order = await orderDao.findById(ctx.params.id);

    if (!order) {
      ctx.status = 404;
      ctx.body = "Order not found";
      return;
    }

    ctx.status = 200;
    ctx.body = order;
  } catch (error) {
    ctx.status = 500;
    ctx.body = "Internal Server Error";
  }
};

/**
 * @param {Koa.Context} ctx
 */
const createOrder = async (ctx) => {
  try {
    const order = await orderDao.create(ctx.request.body);

    ctx.status = 201;
    ctx.body = order;
  } catch (error) {
    ctx.status = 500;
    ctx.body = "Internal Server Error";
  }
};

module.exports = {
  updateOrder,
  getOrderById,
  createOrder,
};
