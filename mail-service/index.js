const Koa = require("koa");
const Router = require("koa-router");
const dotenv = require("dotenv");
const amqp = require("amqplib");
const { sendMail } = require("./utils/send-mail");

dotenv.config();

let channel;
async function connect() {
  const connection = await amqp.connect("amqp://localhost");
  channel = await connection.createChannel();
  await channel.assertQueue("send-email");
}

connect().then(() => {
  channel.consume("send-email", (data) => {
    const { email } = JSON.parse(data.content);
    sendMail(email);
    channel.ack(data);
  });
});

const app = new Koa();
const router = new Router();

router.get("/", (ctx) => {
  ctx.body = "Email Service";
});

app.use(router.routes());
app.use(router.allowedMethods());

const port = process.env.PORT || 5004;
app.listen(port, () => {
  console.log(`Email service listening on port: ${port}`);
});
