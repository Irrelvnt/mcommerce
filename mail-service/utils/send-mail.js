const nodemailer = require("nodemailer");

const sendMail = async (email) => {
  const senderMail = process.env.EMAIL;
  const senderPassword = process.env.PASSWORD;
  if (!senderMail || !senderPassword) {
    throw new Error("Sender mail or password not found");
  }
  const transporter = nodemailer.createTransport({
    service: "Gmail",
    auth: {
      user: senderMail,
      pass: senderPassword,
    },
  });

  const mailOptions = {
    from: senderMail,
    to: email,
    subject: "Payment received",
    text: "Payment received succesfully!",
  };
  try {
    const info = await transporter.sendMail(mailOptions);
    console.log("Message sent: %s", info.messageId);
  } catch (error) {
    console.error("Error sending email:", error.message);
  }
};
module.exports = { sendMail };
