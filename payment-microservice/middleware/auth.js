
const authMiddleware = async (ctx, next) => {
  const { username, password } = ctx.request.body;

  if (username === 'user' && password === 'password') {
    await next();
  } else {
    ctx.status = 401;
    ctx.body = 'Unauthorized';
  }
};

module.exports = authMiddleware;
