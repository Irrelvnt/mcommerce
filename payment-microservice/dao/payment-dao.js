// /src/dao/ProductDao.ts
const axios = require("axios");
const PaymentModel = require("../models/payment");

class PaymentDao {
  create(order) {
    return PaymentModel.create(order);
  }
  async getOrder(orderId) {
    const order = await axios
      .get(`http://localhost:5002/orders/${orderId}`)
      .then((res) => {
        if (res.status === 200) {
          return res.data;
        } else {
          return null;
        }
      })
      .catch(() => null);
    return order;
  }
  async updateOrder(orderId, isCompleted) {
    const isSuccess = await axios
      .put(`http://localhost:5002/orders/${orderId}`, isCompleted)
      .then((res) => {
        if (res.status === 200) {
          return true;
        } else {
          return false;
        }
      })
      .catch(() => false);
    return isSuccess;
  }
}

module.exports = new PaymentDao();
