const Koa = require("koa");
const paymentDao = require("../dao/payment-dao");
const amqp = require("amqplib");

/**
 * @param {Koa.Context} ctx
 */
let channel;
const connect = async () => {
  const connection = await amqp.connect("amqp://localhost");
  channel = await connection.createChannel();
  await channel.assertQueue("send-email");
};
connect();

const payOrder = async (ctx) => {
  const { email, orderId } = ctx.request.body;
  if (!orderId || !email) {
    ctx.status = 400;
    ctx.body = { message: "Order information missing" };
    return;
  }
  const order = await paymentDao.getOrder(orderId);
  if (!order) {
    ctx.status = 404;
    ctx.body = { message: "Order not found" };
    return;
  }
  const payment = await paymentDao.create({
    orderId,
    email,
  });
  if (!payment) {
    ctx.status = 500;
    ctx.body = { message: "Payment failed" };
    return;
  }
  // update order
  const isSuccess = await paymentDao.updateOrder(orderId, {
    isCompleted: true,
  });
  if (!isSuccess) {
    ctx.status = 500;
    ctx.body = { message: "Payment failed" };
    return;
  }
  ctx.status = 200;
  ctx.body = { message: "Order paid" };
  if (email) {
    channel.sendToQueue(
      "send-email",
      Buffer.from(
        JSON.stringify({
          email,
        })
      )
    );
  }
};

module.exports = {
  payOrder,
};
