const koa = require("koa");
const bodyParser = require("koa-bodyparser");
const dotenv = require("dotenv");
const paymentRoutes = require("./routes/payment-routes");
const mongoose = require("mongoose");

const app = new koa();
dotenv.config();

mongoose.connect(process.env.MONGO_URI || "mongodb://localhost:27017/payment");

const db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.once("open", () => {
  console.log("Connected to MongoDB");
});

app.use(bodyParser());

app.use(paymentRoutes.routes()).use(paymentRoutes.allowedMethods());

const PORT = process.env.PORT || 5003;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
