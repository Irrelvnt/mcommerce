const Router = require("koa-router");
const amqp = require("amqplib");
const { payOrder } = require("../controllers/payment-controller");
const authMiddleware = require("../middleware/auth");

const router = new Router();

router.use(authMiddleware).post("/payment", payOrder);

module.exports = router;
