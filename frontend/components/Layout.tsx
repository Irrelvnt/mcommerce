import React from "react";
export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <div className="w-full min-h-screen">
      <div className="h-10 w-full bg-slate-600 px-6 flex items-center fixed top-0 z-50">
        <p className="text-white font-semibold">Mcommerce</p>
      </div>
      {children}
    </div>
  );
}
