import { useEffect, useState } from "react";
import Product from "../types/product";
import axios from "axios";
import Layout from "../components/Layout";

const fetchProducts = async (): Promise<Product[]> => {
  const response = await axios.get("http://localhost:5000/products");
  return response.data;
};

export default function Home() {
  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    fetchProducts().then((products) => {
      setProducts(products);
    });
  }, []);

  return (
    <Layout>
      <div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
        <h2 className="sr-only">Products</h2>

        <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
          {products.map((product, idx) => (
            <a
              key={idx}
              href={`/product/${product._id}`}
              className="border-white hover:border-gray-500 border-2 rounded-lg p-2 transition hover:bg-slate-200"
            >
              <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-lg xl:aspect-h-8 xl:aspect-w-7">
                <img
                  src={product.image}
                  className="h-56 w-full object-contain bg-white"
                />
              </div>
              <h3 className="mt-4 text-sm text-gray-700">{product.name}</h3>
              <p className="mt-1 text-lg font-medium text-gray-900">
                {product.price}
              </p>
            </a>
          ))}
        </div>
      </div>
    </Layout>
  );
}
