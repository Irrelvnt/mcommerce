import { useEffect, useMemo, useState } from "react";
import { FaCheckCircle, FaStar } from "react-icons/fa";
import { FaShieldAlt } from "react-icons/fa";
import axios from "axios";
import Product from "../types/product";
import { useParams } from "react-router-dom";
import Layout from "../components/Layout";
import LoginModal from "../components/LoginModal";

type orderInformations = {
  productId: string;
  orderDate: Date;
};

const fetchProduct = async (productId: string) => {
  const res = await axios.get(`http://localhost:5000/products/${productId}`);
  return res.data as Product;
};
const reviews = { average: 4, totalCount: 1624 };

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

const makeOrder = async (orderInformations: orderInformations) => {
  const res = await axios.post(
    `http://localhost:5000/orders`,
    orderInformations
  );
  if (res.status === 201) return res.data;
  return null;
};

export default function ProductPage() {
  const [product, setProduct] = useState<Product | null>(null);
  const [orderId, setOrderId] = useState<string>("");
  const [open, setOpen] = useState<boolean>(false);
  const { productId } = useParams();

  const handleOrder = useMemo(() => {
    return () => {
      const orderInformations: orderInformations = {
        productId: `${productId}`,
        orderDate: new Date(),
      };
      makeOrder(orderInformations).then((order) => {
        if (order) {
          setOrderId(order._id);
        }
      });
    };
  }, [productId]);

  useEffect(() => {
    if (orderId.length > 0) {
      console.log(orderId);
      setOpen(true);
    }
  }, [orderId]);

  useEffect(() => {
    fetchProduct(`${productId}`).then((product: Product) =>
      setProduct(product)
    );
  }, [productId]);

  if (!product) {
    return (
      <div className="w-full h-screen flex items-center justify-center">
        <div className="animate-spin border-b-2 border-b-gray-800 rounded-full w-12 h-12" />
      </div>
    );
  }
  return (
    <Layout>
      <LoginModal open={open} setOpen={setOpen} orderId={orderId} />
      <div className="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8 lg:grid lg:grid-cols-2 lg:gap-x-8">
        {/* Product details */}
        <div className="lg:max-w-lg lg:self-end">
          <div className="mt-4">
            <h1 className="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">
              {product?.name}
            </h1>
          </div>

          <section aria-labelledby="information-heading" className="mt-4">
            <h2 id="information-heading" className="sr-only">
              Product information
            </h2>

            <div className="flex items-center">
              <p className="text-lg text-gray-900 sm:text-xl">
                {product?.price}
              </p>

              <div className="ml-4 pl-4 border-l border-gray-300">
                <h2 className="sr-only">Reviews</h2>
                <div className="flex items-center">
                  <div>
                    <div className="flex items-center">
                      {[0, 1, 2, 3, 4].map((rating) => (
                        <FaStar
                          key={rating}
                          className={classNames(
                            reviews.average > rating
                              ? "text-yellow-400"
                              : "text-gray-300",
                            "h-5 w-5 flex-shrink-0"
                          )}
                          aria-hidden="true"
                        />
                      ))}
                    </div>
                    <p className="sr-only">{reviews.average} out of 5 stars</p>
                  </div>
                  <p className="ml-2 text-sm text-gray-500">
                    {reviews.totalCount} reviews
                  </p>
                </div>
              </div>
            </div>

            <div className="mt-4 space-y-6">
              <p className="text-base text-gray-500">{product?.description}</p>
            </div>

            <div className="mt-6 flex items-center">
              <FaCheckCircle
                className="flex-shrink-0 w-5 h-5 text-green-500"
                aria-hidden="true"
              />
              <p className="ml-2 text-sm text-gray-500">
                In stock and ready to ship
              </p>
            </div>
          </section>
        </div>

        {/* Product image */}
        <div className="mt-10 lg:mt-0 lg:col-start-2 lg:row-span-2 lg:self-center">
          <div className="rounded-lg overflow-hidden">
            <img
              src={product?.image}
              className="w-full h-[80vh] object-contain"
            />
          </div>
        </div>

        {/* Product form */}
        <div className="mt-10 lg:max-w-lg lg:col-start-1 lg:row-start-2 lg:self-start">
          <section aria-labelledby="options-heading">
            <h2 id="options-heading" className="sr-only">
              Product options
            </h2>

            <div className="mt-10">
              <button
                onClick={handleOrder}
                className="w-full bg-indigo-600 border border-transparent rounded-md py-3 px-8 flex items-center justify-center text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-50 focus:ring-indigo-500"
              >
                Add to bag
              </button>
            </div>
            <div className="mt-6 text-center">
              <a href="#" className="group inline-flex text-base font-medium">
                <FaShieldAlt
                  className="flex-shrink-0 mr-2 h-6 w-6 text-gray-400 group-hover:text-gray-500"
                  aria-hidden="true"
                />
                <span className="text-gray-500 hover:text-gray-700">
                  Lifetime Guarantee
                </span>
              </a>
            </div>
          </section>
        </div>
      </div>
    </Layout>
  );
}
