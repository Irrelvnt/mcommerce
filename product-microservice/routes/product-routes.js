const Router = require("koa-router");

const {
  getProducts,
  getProductById,
  createProducts,
} = require("../controllers/product-controller");

const router = new Router();

router.get("/products", getProducts);
router.post("/products", createProducts);
router.get("/products/:id", getProductById);

module.exports = router;
