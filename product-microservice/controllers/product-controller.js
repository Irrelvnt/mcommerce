const Koa = require("koa");
const productDao = require("../dao/product-dao");

/**
 * @param {Koa.Context} ctx
 */

const getProducts = async (ctx) => {
  try {
    const products = await productDao.findAll();

    if (products.length === 0) {
      ctx.status = 404;
      ctx.body = "No Products found";
    }

    ctx.body = products;
  } catch (error) {
    ctx.status = 500;
    ctx.body = "Internal Server Error";
  }
};

/**
 * @param {Koa.Context} ctx
 */
const getProductById = async (ctx) => {
  try {
    const product = await productDao.findById(ctx.params.id);

    if (!product) {
      ctx.status = 404;
      ctx.body = "Product not found";
      return;
    }

    ctx.status = 200;
    ctx.body = product;
  } catch (error) {
    console.log(error);
    ctx.status = 500;
    ctx.body = "Internal Server Error";
  }
};

/**
 * @param {Koa.Context} ctx
 */
const createProducts = async (ctx) => {
  try {
    const product = await productDao.createMultiple(ctx.request.body);

    ctx.status = 201;
    ctx.body = product;
  } catch (error) {
    console.log(error);
    ctx.status = 500;
    ctx.body = "Internal Server Error";
  }
};

module.exports = {
  getProducts,
  getProductById,
  createProducts,
};
