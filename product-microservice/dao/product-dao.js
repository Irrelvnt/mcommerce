const ProductModel = require("../models/product");

class ProductDao {
  findAll() {
    return ProductModel.find().exec();
  }

  findById(id) {
    return ProductModel.findById(id).exec();
  }
  createMultiple(products) {
    return ProductModel.create(products);
  }
}

module.exports = new ProductDao();
