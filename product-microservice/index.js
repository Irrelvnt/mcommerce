const koa = require("koa");
const bodyParser = require("koa-bodyparser");
const dotenv = require("dotenv");
const productRoutes = require("./routes/product-routes");
const mongoose = require("mongoose");

const app = new koa();
dotenv.config();

mongoose.connect(process.env.MONGO_URI || "mongodb://localhost:27017/product");

const db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.once("open", () => {
  console.log("Connected to MongoDB");
});

app.use(bodyParser());

app.use(productRoutes.routes()).use(productRoutes.allowedMethods());

const PORT = process.env.PORT || 5001;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
