const koa = require("koa");
const proxy = require("koa-proxies");
const KoaRatelimit = require("koa-ratelimit");
const morgan = require("koa-morgan");
const cors = require("@koa/cors");

const app = new koa();

app.use(
  cors({
    origin: ["*"],
  })
);

const ports = {
  "/products": 5001,
  "/orders": 5002,
  "/payment": 5003,
};

app.use(morgan("combined"));
for (const [service, port] of Object.entries(ports)) {
  app.use(proxy(service, { target: `http://localhost:${port}` }));
}

const rateLimit = KoaRatelimit({
  driver: "memory",
  duration: 60000,
  max: 5,
});
app.use(rateLimit);

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Mcommerce API is running on port ${PORT}`);
});
